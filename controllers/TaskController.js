// Controllers contain the functions and business logic of our Express JS app.
// Meaning all the operation it can perform will be written in this file.
const Task = require("../models/Task.js");



// Controller Functions:
module.exports.getAllTasks = () => {
	return Task.find({}).then(result => {
		return result;
	})
};


// Function for GETTING SPECIFIC task
module.exports.getOneTasks = (taskId) => {
	console.log(taskId)
	return Task.findById(taskId).then((foundTask, error) => {
		if(error) {
			console.log(error)
			return false;
		} else {
			return foundTask;
		}
	})
};


// Function for CREATING tasks
module.exports.createTask = (reqBody) => {
	let newTask = new Task({
		name: reqBody.name
	});

	return newTask.save().then((savedTask, error) => {
		if(error){
			console.log(error)
			return false
		} else if (savedTask != null && savedTask.name == reqBody.name){
			return "Duplicate Task Found!"	
		} else {
			console.log(savedTask)
			return savedTask
		}

	})
};


// Function for UPDATING tasks using PUT method
module.exports.completeTask = (taskId, newContent) => {
    return Task.findById(taskId).then((result, error) => {
        if (error) {
            console.log(error)
            return false
        }
        result.status = "Completed"
        return result.save().then((completedTask, error) => {
            if (error) {
                console.log(error)
                return false
            } else {
                return completedTask
            }

        })
    })
}




// Function for UPDATING tasks
module.exports.updateTask = (taskId, newContent) => {
	return Task.findById(taskId).then((result, error) => {
		if(error){
			console.log(error);
			return false
		}
			result.name = newContent.name

			return result.save().then((updatedTask, error) => {
				if(error){
					console.log(error)
					return false
				} else {
					return updatedTask
				}
			}) 
	})
};


// Function for DELETING tasks
module.exports.deleteTask = (taskId) => {
	return Task.findByIdAndRemove(taskId).then((deletedTask, error) => {
		if(error){
			return false // "Cannot Delete Task" or "An Error has occured"
		} else {
			return "Task Deleted"
		}
	})
};
